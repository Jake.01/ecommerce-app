import "./App.css";
import { UserProvider } from "./UserContext";
import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Error from "./pages/Error.js";
import Footer from "./pages/Footer.js";
import Home from "./pages/Home.js";
import Login from "./pages/Login.js";
import AppNavbar from "./components/AppNavbar.js";
import Register from "./components/Register.js";
import ProductCatalog from "./pages/ProductCatalog.js";
import Products from "./pages/Products.js";
import AdminDashboard from "./pages/AdminDashboard.js";
import ProductView from "./pages/ProductView.js";
import Logout from "./pages/Logout.js";
import ProductCategory from "./components/ProductCategory.js";
function App() {
  const [user, setUser] = useState({ id: null, isAdmin: null });

  //function for clearing localstorage

  const unsetUser = () => {
    localStorage.clear();
  };
  // console.log(user)

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id !== undefined) {
          setUser({ id: data._id, isAdmin: data.isAdmin });
        } else {
          setUser({ id: null, isAdmin: null });
        }
      });
    // Token will be sent as part of the request's header.
  }, []);

  return (
    //We store information in the context by providing the information using the "UserProvider" component and passing the information via the "value" prop.
    // all the information inside the table
    <UserProvider
      value={{
        user,
        setUser,
        unsetUser,
      }}
    >
      {/* Router component is used to wrapped around all componenets which will have access to the routing system
       */}

      <Router>
        <AppNavbar />
        <Container>
          {/* Routes holds all our Route components. */}
          <Routes>
            <>
              {/*
          Route asssign an endpoint and display the appropriate page component for that endpoint.
          - exact and patch props to assign the endpoint and page should be only accessed on the specific endpoint
          -"element" props assign page components to the displayed enpoint.*/}
              <Route exact="exact" path="/admin" element={<AdminDashboard />} />
              <Route
                exact="exact"
                path="/productCategory"
                element={<ProductCategory />}
              />
              <Route
                exact="exact"
                path="/products/:productId"
                element={<ProductView />}
              />
              <Route exact="exact" path="/logout" element={<Logout />} />
              <Route exact="exact" path="/" element={<Home />} />

              <Route path="*" element={<Error />} />
            </>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}
export default App;
