import { useState, useEffect } from "react";
import { Row, Col, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
export default function ProductCard({ productProp }) {
  const { _id, name, description, price, stock } = productProp;

  return (
    <Row>
      <Card className="my-3">
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle className="mb-2 text-muted">
            Description:
          </Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle className="mb-2 text-muted">price:</Card.Subtitle>
          <Card.Text>Php {price}</Card.Text>
          <Card.Subtitle>Slots:</Card.Subtitle>
          <Card.Text>
            {stock}
            available
          </Card.Text>
          <Button as={Link} to={`/product/${_id}`} variant="primary">
            details
          </Button>
        </Card.Body>
      </Card>
    </Row>
  );
}
