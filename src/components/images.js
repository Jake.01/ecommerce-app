const images = [
  {
    id: 1,
    src: "../images/carouselitem1.jpeg",
    alt: "Image 1",
  },
  {
    id: 2,
    src: "../images/carouselitem2.jpg",
    alt: "Image 2 ",
  },
  {
    id: 3,
    src: "../images/carouselitem3.jpg",
    alt: "Image 3",
  },
];
export default images;
