import { react, useState, useEffect } from "react";
import { Row, Col, Card, Button, Accordion } from "react-bootstrap";
import { Link } from "react-router-dom";
import Categories from "./Categories";

export default function ProductCategory({}) {
  const [data, setData] = useState([]);
  const [allProducts, setAllProducts] = useState([]);

  const fetchData = async () => {
    // get all the products from the database
    await fetch(`${process.env.REACT_APP_API_URL}/products`, {})
      .then((res) => res.json())
      .then((data) => {
        setAllProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);
  console.log(allProducts);

  const filterResult = async (category) => {
    await fetch(`${process.env.REACT_APP_API_URL}/products`, {})
      .then((res) => res.json())
      .then((data) => {
        const result = data.filter((curData) => {
          if (curData.stock >= 1) {
            return curData.category == category;
          }
        });
        setAllProducts(result);
      });

    // const result = allProducts.filter((curData) => {
    //   return curData.category == category;
    // });
    // setAllProducts(result);
  };

  const filterPrice = async (priceStart, priceEnd) => {
    await fetch(`${process.env.REACT_APP_API_URL}/products`, {})
      .then((res) => res.json())
      .then((data) => {
        const result = data.filter((curData) => {
          if (curData.price >= priceStart && curData.stock >= 1) {
            return curData.price <= priceEnd;
          }
        });
        setAllProducts(result);
      });
  };

  const filterStock = async (category) => {
    await fetch(`${process.env.REACT_APP_API_URL}/products`, {})
      .then((res) => res.json())
      .then((data) => {
        const result = data.filter((curData) => {
          return curData.stock >= 1;
        });
        setAllProducts(result);
      });

    // const result = allProducts.filter((curData) => {
    //   return curData.category == category;
    // });
    // setAllProducts(result);
  };

  const filterSoldOut = async (category) => {
    await fetch(`${process.env.REACT_APP_API_URL}/products`, {})
      .then((res) => res.json())
      .then((data) => {
        const result = data.filter((curData) => {
          return curData.stock <= 0;
        });
        setAllProducts(result);
      });

    // const result = allProducts.filter((curData) => {
    //   return curData.category == category;
    // });
    // setAllProducts(result);
  };

  return (
    <div className="container-fluid mx-2">
      <div className="row mt-5 mx-2">
        <h1>Products</h1>
        <div className="col-md-3">
          <Accordion defaultActiveKey="1">
            <Accordion.Item eventKey="1">
              <Accordion.Header>Categories</Accordion.Header>
              <Accordion.Body>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => {
                    filterResult("PROCESSOR");
                  }}
                >
                  Processor
                </button>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => {
                    filterResult("GPU");
                  }}
                >
                  Graphic Card
                </button>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => filterResult("MONITOR")}
                >
                  Monitor
                </button>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => filterResult("STORAGE")}
                >
                  Storage
                </button>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => fetchData()}
                >
                  Show All
                </button>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
          <Accordion defaultActiveKey="1">
            <Accordion.Item eventKey="1">
              <Accordion.Header>Availability</Accordion.Header>
              <Accordion.Body>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => {
                    filterPrice(50000, 999999999);
                  }}
                >
                  50,000 - above
                </button>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => {
                    filterPrice(25000, 49999);
                  }}
                >
                  25,000 - 49,999
                </button>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => {
                    filterPrice(1, 24999);
                  }}
                >
                  1 - 25,000
                </button>

                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => {
                    filterStock();
                  }}
                >
                  Available
                </button>
                <button
                  className="btn btn-secondary w-100 mb-3"
                  onClick={() => {
                    filterSoldOut();
                  }}
                >
                  Sold Out
                </button>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </div>
        <div className="col-md-9 text-center">
          <div className="row">
            {allProducts.map((products) => {
              return (
                <>
                  <div className="col-md-4 mb-4" key={products._id}>
                    <div className="card">
                      <img
                        className="card-img-top"
                        src={`../images/${products.image}.png`}
                        alt="Card image cap"
                      />
                      <div className="card-body">
                        <h5 className="card-title">{products.productName}</h5>
                        <p></p>
                        <p className="card-text">
                          Php {products.price.toLocaleString()}
                        </p>
                        {products.stock > 0 ? (
                          <Button
                            as={Link}
                            to={`/products/${products._id}`}
                            className="btn btn-primary"
                          >
                            Buy Now!
                          </Button>
                        ) : (
                          <Button
                            as={Link}
                            to={`/products/${products._id}`}
                            className="btn btn-danger disabled"
                          >
                            Sold out
                          </Button>
                        )}
                      </div>
                    </div>
                  </div>
                </>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

/*  return (
    <>
      <h1 className="text-center text-info">Let's Shop</h1>
      <div className="container-fluid mx-2">
        <div className="row mt-5 mx-2">
          <div className="col-md-3">
            <button className="btn btn-secondary w-100 mb-3">Processor</button>
            <button className="btn btn-secondary w-100 mb-3">Graphic Card</button>
            <button className="btn btn-warning w-100 mb-3">Monitor</button>
            <button className="btn btn-warning w-100 mb-3">Storage</button>
          </div>
          <div className="col-md-9">
            <div className="row">
              <div className="col-md-4 mb-4">
                <div className="card">
                  <img
                    className="card-img-top"
                    src={require("../images/ROGLAPTOP.png")}
                    alt="Card image cap"
                  />
                  <div className="card-body">
                    <h5 className="card-title">{}</h5>
                    <p></p>
                    <p className="card-text">PHP {}</p>
                    <a href="#" className="btn btn-primary">
                      Buy Now!
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}*/
