import ProductCategory from "../components/ProductCategory.js";
// import coursesData from "../data/CoursesData.js";
import { Navigate } from "react-router-dom";

import { useEffect, useState, useContext } from "react";
import UserContext from "../UserContext.js";
export default function Products() {
  const { user } = useContext(UserContext);
  const [products, setProducts] = useState([]);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProducts(
          data.map((products) => {
            return (
              <ProductCategory key={products._id} productProp={products} />
            );
          })
        );
      });
  }, []);

  return user.isAdmin ? (
    <Navigate to="/admin" />
  ) : (
    <>
      <h1> Products</h1>
      {products}
    </>
  );
}
