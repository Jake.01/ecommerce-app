import { useContext, useEffect, useState } from "react";

import { Button, Table, Modal, Form } from "react-bootstrap";
import { useParams, Navigate, Link } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function AdminDashboard() {
  const { user } = useContext(UserContext);

  // Create allProducts state to contain the products from the response of our fetch data.
  const [allProducts, setAllProducts] = useState([]);

  const [allOrders, setAllOrders] = useState([]);
  const [allUsers, setAllUsers] = useState([]);

  // State hooks to store the values of the input fields for our modal.
  const [productId, setProductId] = useState("");
  const [productName, setProductName] = useState("");
  const [productCode, setProductCode] = useState("");
  const [description, setDescription] = useState("");
  const [category, setCategory] = useState("");
  const [image, setImage] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [productIdSpecific, setProductIdSpecific] = useState("");
  const [productNameSpecific, setProductNameSpecific] = useState("");
  const [descriptionSpecific, setDescriptionSpecific] = useState("");
  const [imageSpecific, setImageSpecific] = useState("");
  const [priceSpecific, setPriceSpecific] = useState(0);

  // State to determine whether submit button in the modal is enabled or not
  const [isActive, setIsActive] = useState(false);

  // State for Add/Edit Modal
  const [showAdd, setShowAdd] = useState(false);
  const [showEdit, setShowEdit] = useState(false);
  const [showUser, setShowUser] = useState(false);
  const [showProduct, setShowProduct] = useState(false);

  // To control the add product modal pop out
  const openAdd = () => setShowAdd(true); //Will show the modal
  const closeAdd = () => setShowAdd(false); //Will hide the modal

  // To control the add product modal pop out
  const openUser = () => setShowUser(true); //Will show the modal
  const closeUser = () => setShowUser(false); //Will hide the modal

  // To control the add product modal pop out
  const openProduct = () => setShowProduct(true); //Will show the modal
  const closeProduct = () => setShowProduct(false); //Will hide the modal

  // To control the edit product modal pop out
  // We have passed a parameter from the edit button so we can retrieve a specific product and bind it with our input fields.

  const openEdit = (id) => {
    setProductId(id);

    // Getting a specific product to pass on the edit modal
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        // updating the product states for editing
        setProductName(data.productName);
        setDescription(data.description);
        setPrice(data.price);
        setStock(data.stock);
        setProductCode(data.productCode);
        setCategory(data.category);
        setImage(data.image);
      });

    setShowEdit(true);
  };

  const closeEdit = () => {
    // Clear input fields upon closing the modal
    setProductName("");
    setDescription("");
    setProductCode("");
    setCategory("");
    setImage("");
    setPrice(0);
    setStock(0);

    setShowEdit(false);
  };

  // [SECTION] To view all product in the database (active & inactive)
  // fetchData() function to get all the active/inactive products.
  const fetchData = () => {
    // get all the products from the database
    fetch(`${process.env.REACT_APP_API_URL}/products/getProductsAdmin`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setAllProducts(
          data.map((product) => {
            return (
              <tr key={product._id}>
                <td>{product.productCode}</td>
                <td>{product._id}</td>
                <td>{product.productName}</td>
                <td>{product.description.substring(0, 100)}</td>
                <td>{product.price}</td>
                <td>{product.stock}</td>
                <td>{product.isActive ? "Active" : "Inactive"}</td>
                <td>
                  {
                    // conditonal rendering on what button should be visible base on the status of the product
                    product.isActive ? (
                      <Button
                        className="m-2"
                        variant="danger"
                        size="sm"
                        onClick={() =>
                          archive(product._id, product.productName)
                        }
                      >
                        Archive
                      </Button>
                    ) : (
                      <>
                        <Button
                          className="m-2"
                          variant="success"
                          size="sm"
                          className="mx-1"
                          onClick={() =>
                            unarchive(product._id, product.productName)
                          }
                        >
                          Unarchive
                        </Button>
                        <Button
                          className="m-2"
                          variant="secondary"
                          size="sm"
                          className="mx-1"
                          onClick={() => openEdit(product._id)}
                        >
                          Edit
                        </Button>
                      </>
                    )
                  }
                </td>
                <td>
                  <Button
                    onClick={() => viewProduct(product._id)}
                    className="m-2"
                    variant="primary"
                    size="sm"
                  >
                    View
                  </Button>
                </td>
              </tr>
            );
          })
        );
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  //Section get all user

  const fetchUser = () => {
    // get all the products from the database
    fetch(`${process.env.REACT_APP_API_URL}/users`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setAllUsers(
          data.map((user) => {
            return (
              <tr key={user._id}>
                <td>{user.email}</td>
                <td>{user.isAdmin ? `Admin` : `Customer`}</td>
                <td>
                  {
                    // conditonal rendering on what button should be visible base on the status of the product
                    user.isAdmin ? (
                      <Button
                        className="m-2"
                        variant="danger"
                        size="sm"
                        disabled
                      >
                        Admin
                      </Button>
                    ) : (
                      <>
                        <Button
                          className="m-2"
                          variant="success"
                          size="sm"
                          className="mx-1"
                          onClick={() => changeAdmin(user._id, user.email)}
                        >
                          Change to Admin
                        </Button>
                      </>
                    )
                  }
                </td>
              </tr>
            );
          })
        );
      });
  };

  useEffect(() => {
    fetchUser();
  }, []);

  const changeAdmin = (id, email) => {
    console.log(id);
    console.log(email);

    // Using the fetch method to set the isActive property of the product document to false
    fetch(`${process.env.REACT_APP_API_URL}/users/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isAdmin: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Change to admin Succesfully",
            icon: "success",
            text: `${email} is now admin.`,
          });
          // To show the update with the specific operation intiated.
          fetchUser();
        } else {
          Swal.fire({
            title: "Unarchive Unsuccessful",
            icon: "error",
            text: "Something went wrong. Please try again later!",
          });
        }
      });
  };

  // [SECTION] - Get all Orders.

  // const Component = () => {

  //   useEffect(() => {
  //     const fetchOrder = async () => {
  //       fetch(`${process.env.REACT_APP_API_URL}/products/getAllOrders`, {
  //         headers: {
  //           Authorization: `Bearer ${localStorage.getItem("token")}`,
  //         },
  //       })
  //         .then((res) => res.json())
  //         .then((data) => {
  //           setAllOrders(fetchOrder);
  //           console.log(fetchOrder);
  //         });
  //     };
  //     fetchOrder();
  //   }, []);
  //   //
  //   return (
  //     <ul>
  //       {Object.keys(allOrders || []).map((product) => (
  //         <li>{product.order}</li>
  //       ))}
  //     </ul>
  //   );
  // };
  //   // const [allOrders, setAllOrders] = useState();
  //
  // const fetchOrder = () => {
  //   // get all the products from the database
  //   fetch(`${process.env.REACT_APP_API_URL}/products/getAllOrders`, {
  //     headers: {
  //       Authorization: `Bearer ${localStorage.getItem("token")}`,
  //     },
  //   })
  //     .then((res) => res.json())
  //     .then((data) => {
  //       console.log(data);
  //       setAllOrders(
  //         Object.keys(data).map((data) => {
  //           console.log(data);
  //           return (
  //             <>
  //               <tr key={data._id}>
  //                 <td>{data.orders}</td>
  //               </tr>
  //             </>
  //           );
  //         })
  //       );
  //     });
  // };
  //
  // // to fetch all products in the first render of the page.
  // useEffect(() => {
  //   fetchOrder();
  // }, []);

  // [SECTION] Setting the product to Active/Inactive

  // Making the product inactive
  const archive = (id, productName) => {
    console.log(id);
    console.log(productName);

    // Using the fetch method to set the isActive property of the product document to false
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Archive Successful",
            icon: "success",
            text: `${productName} is now inactive.`,
          });
          // To show the update with the specific operation intiated.
          fetchData();
        } else {
          Swal.fire({
            title: "Archive unsuccessful",
            icon: "error",
            text: "Something went wrong. Please try again later!",
          });
        }
      });
  };

  // Making the product active
  const unarchive = (id, productName) => {
    console.log(id);
    console.log(productName);

    // Using the fetch method to set the isActive property of the product document to false
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Unarchive Successful",
            icon: "success",
            text: `${productName} is now active.`,
          });
          // To show the update with the specific operation intiated.
          fetchData();
        } else {
          Swal.fire({
            title: "Unarchive Unsuccessful",
            icon: "error",
            text: "Something went wrong. Please try again later!",
          });
        }
      });
  };

  // [SECTION] Adding a new product
  // Inserting a new product in our database
  const addProduct = (e) => {
    // Prevents page redirection via form submission
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        price: price,
        stock: stock,
        productCode: productCode,
        category: category,
        image: image,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Product succesfully Added",
            icon: "success",
            text: `${productName} is now added`,
          });

          // To automatically add the update in the page
          fetchData();
          // Automatically closed the modal
          closeAdd();
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: `Something went wrong. Please try again later!`,
          });
          closeAdd();
        }
      });

    // Clear input fields
    setProductName("");
    setDescription("");
    setProductCode("");
    setCategory("");
    setImage("");
    setPrice(0);
    setStock(0);
  };

  // [SECTION] Edit a specific product
  // Updating a specific product in our database
  // edit a specific product
  const editProduct = (e) => {
    // Prevents page redirection via form submission
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        price: price,
        stock: stock,
        image: image,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: "Product succesfully Updated",
            icon: "success",
            text: `${productName} is now updated`,
          });

          // To automatically add the update in the page
          fetchData();
          // Automatically closed the form
          closeEdit();
        } else {
          Swal.fire({
            title: "Error!",
            icon: "error",
            text: `Something went wrong. Please try again later!`,
          });

          closeEdit();
        }
      });

    // Clear input fields
    setProductName("");
    setDescription("");
    setProductCode("");
    setCategory("");
    setImage("");
    setPrice(0);
    setStock(0);
  };

  const viewProduct = (id) => {
    console.log(id);
    console.log(productName);

    // Using the fetch method to set the isActive property of the product document to false
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setProductIdSpecific(data.productId);
        setProductNameSpecific(data.productName);
        setDescriptionSpecific(data.description);
        setPriceSpecific(data.price);
        setImageSpecific(data.image);
        setShowProduct(true);
      });
  };
  // Submit button validation for add/edit product
  useEffect(() => {
    // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
    if (productName !== "" && description !== "" && price > 0 && stock > 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [productName, description, price, stock]);
  console.log(user);
  return user.isAdmin ? (
    <>
      {/*Header for the admin dashboard and functionality for create product and show enrollments*/}
      <div className="mt-5 mb-3 text-center">
        <h1>Admin Dashboard</h1>
        {/*Adding a new product */}
        <Button
          variant="success"
          className="
				mx-2"
          onClick={openAdd}
        >
          Add Product
        </Button>
        {/*To view all the user enrollments*/}
        <Button
          variant="secondary"
          className="
				mx-2"
          onClick={openUser}
        >
          Show Users
        </Button>
      </div>
      {/*End of admin dashboard header*/}
      {/*For view all the products in the database.*/}
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Product Code</th>
            <th>Product ID</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Status</th>
            <th>Actions</th>
            <th>View</th>
          </tr>
        </thead>
        <tbody>{allProducts}</tbody>
      </Table>
      {/*End of table for product viewing*/}
      <Modal
        className="modal-xl"
        show={showProduct}
        fullscreen={false}
        onHide={closeProduct}
      >
        <Modal.Header closeButton>
          <Modal.Title>Product Preview</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container-fluid">
            <div className="row mt-5 mx-2">
              <div className="col-md-5 mt-5 col-xs-12">
                <img
                  width="400"
                  className="card img-fluid"
                  src={`../images/${imageSpecific}.png`}
                  alt="Card image cap"
                />
              </div>
              <div className="col-md-5 col-xs-12">
                <h2 className="text-center mb-4">{productNameSpecific}</h2>
                <h6>Quick Overview: {descriptionSpecific}</h6>
                <hr />
                <h1 className="color-green">
                  Php: {priceSpecific.toLocaleString()}
                </h1>
                <Form.Control
                  disabled
                  min="1"
                  className="mt-5 w-25"
                  type="number"
                  placeholder="Qty."
                  value=""
                  required
                />

                <Button
                  disabled
                  className="btn-lg btn-success me-5 mt-3"
                  disabled
                >
                  Buy Now!
                </Button>
                <Button
                  disabled
                  onClick={closeProduct}
                  className="btn-lg btn-secondary mt-3"
                >
                  Go back
                </Button>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="primary"
            type="submit"
            id="submitBtn"
            onClick={closeProduct}
          >
            Done
          </Button>
        </Modal.Footer>
      </Modal>
      {/*For view all the users in the database.*/}
      <Modal
        className="modal-xl"
        show={showUser}
        fullscreen={false}
        onHide={closeUser}
      >
        <Modal.Header closeButton>
          <Modal.Title>List of Users</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Email</th>
                <th>Role</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>{allUsers}</tbody>
          </Table>
        </Modal.Body>

        <Modal.Footer>
          <Button
            variant="primary"
            type="submit"
            id="submitBtn"
            onClick={closeUser}
          >
            Done
          </Button>
        </Modal.Footer>
      </Modal>
      {/*End of table for orders viewing*/}
      {/*Modal for Adding a new product*/}
      <Modal show={showAdd} fullscreen={false} onHide={closeAdd}>
        <Form onSubmit={(e) => addProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Add New Product</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="productName" className="mb-3">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Name"
                value={productName}
                onChange={(e) => setProductName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="description" className="mb-3">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter Product Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="price" className="mb-3">
              <Form.Label>Product Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Product Price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="stock" className="mb-3">
              <Form.Label>Product Stock</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Product Stock"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="image" className="mb-3">
              <Form.Label>Image file</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter image link"
                value={image}
                onChange={(e) => setImage(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="productCode" className="mb-3">
              <Form.Label>Product Code</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Code"
                value={productCode}
                onChange={(e) => setProductCode(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="category" className="mb-3">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                required
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            {isActive ? (
              <Button
                variant="primary"
                type="submit"
                id="submitBtn"
                onClick={(e) => addProduct(e)}
              >
                Save
              </Button>
            ) : (
              <Button variant="danger" type="submit" id="submitBtn" disabled>
                Save
              </Button>
            )}
            <Button variant="secondary" onClick={closeAdd}>
              Close
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      {/*End of modal for adding product*/}
      {/*Modal for Editing a product*/}
      <Modal show={showEdit} fullscreen={false} onHide={closeEdit}>
        <Form onSubmit={(e) => editProduct(e)}>
          <Modal.Header closeButton>
            <Modal.Title>Edit a Product</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <Form.Group controlId="productName" className="mb-3">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Name"
                value={productName}
                onChange={(e) => setProductName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="description" className="mb-3">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter Product Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="price" className="mb-3">
              <Form.Label>Product Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Product Price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="stock" className="mb-3">
              <Form.Label>Product Stock</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter Product stock"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="productCode" className="mb-3">
              <Form.Label>Product Code</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Code"
                value={productCode}
                onChange={(e) => setProductCode(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="category" className="mb-3">
              <Form.Label>Category</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group controlId="image" className="mb-3">
              <Form.Label>Image file</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter image link"
                value={image}
                onChange={(e) => setImage(e.target.value)}
                required
              />
            </Form.Group>
          </Modal.Body>

          <Modal.Footer>
            {isActive ? (
              <Button variant="primary" type="submit" id="submitBtn">
                Save
              </Button>
            ) : (
              <Button variant="danger" type="submit" id="submitBtn" disabled>
                Save
              </Button>
            )}
            <Button variant="secondary" onClick={closeEdit}>
              Close
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      {/*End of modal for editing a product*/}
    </>
  ) : (
    <Navigate to="/productCategory" />
  );
}
