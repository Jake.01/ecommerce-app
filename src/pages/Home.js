import Banner from "../components/Banner.js";
import { react, useState, useEffect } from "react";
import "../App.css";
import { Link } from "react-router-dom";
import {
  Carousel,
  Row,
  Container,
  Col,
  Stack,
  Card,
  CardGroup,
  Button,
} from "react-bootstrap";
import "../App.css";

export default function Home() {
  const [data, setData] = useState([]);
  const [allProducts, setAllProducts] = useState([]);

  const fetchData = async (category) => {
    // get all the products from the database
    await fetch(`${process.env.REACT_APP_API_URL}/products`, {})
      .then((res) => res.json())
      .then((data) => {
        const result = data.filter((curData) => {
          if (curData.category == `FEATURED`) {
            return curData.category == `FEATURED`;
          }
        });
        setAllProducts(result);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <img
        className="centerlogo"
        width="200px"
        src={"../logo/logo_transparent.png"}
      />
      <div className=" mt-5">
        <Row>
          <Col xs={8} md={10} className="d-flex">
            <Carousel className="w-100 ">
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={"../images/image3.jpg"}
                  alt="First slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={"../images/carouselitem2.jpg"}
                  alt="Second slide"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={"../images/carouselitem3.jpg"}
                  alt="Third slide"
                />
              </Carousel.Item>
            </Carousel>
          </Col>
          <Col xs={4} md={2}>
            <Stack gap={2} className=" mx-auto d-flex">
              <img
                className="rounded ms-4 mt-5"
                src={"../images/i9.png"}
                alt="Third slide"
              />
              <img
                className="rounded ms-4 mt-5"
                src={"../images/i7.png"}
                alt="Third slide"
              />
            </Stack>
          </Col>
        </Row>
      </div>
      <hr />
      {/*Card Section*/}
      <div className="col-md-3"></div>
      <div className="md-9 mt-5">
        <h1> Featured Products </h1>
        <div className="row">
          {allProducts.map((products) => {
            return (
              <>
                <div className="col-md-4 mb-4 text-center" key={products._id}>
                  <Card>
                    <Card.Img
                      className="h-50"
                      variant="top"
                      src={`../images/${products.image}.png`}
                    />
                    <Card.Body>
                      <Card.Title>{products.productName}</Card.Title>
                      <Card.Text>{`Php ${products.price.toLocaleString()}`}</Card.Text>
                      {products.stock > 0 ? (
                        <Button
                          as={Link}
                          to={`/products/${products._id}`}
                          className="btn btn-primary"
                        >
                          Buy Now!
                        </Button>
                      ) : (
                        <Button
                          as={Link}
                          to={`/products/${products._id}`}
                          className="btn btn-danger disabled"
                        >
                          Sold out
                        </Button>
                      )}
                    </Card.Body>
                  </Card>
                </div>
              </>
            );
          })}
          <div className="marginBottom"></div>
        </div>
      </div>
      <section className="footer">
        <h1 className="text-center m-3 text-secondary">
          Branches and Contacts
        </h1>
        <div className="container">
          <div className="row">
            <div className="col-sm-3">
              <h5 className="text-secondary">Makati</h5>
              <ul className="text-secondary">
                <li>
                  Poblacion Makati
                  <br />
                  9984 Street
                  <br />
                  Brgy. Dell
                  <br />
                  MakatiCity
                </li>
                <br />
                <li>
                  8-888-1234
                  <br />
                  8-999-654
                </li>
                <li>0906-2148748</li>
              </ul>
            </div>
            <div className="col-sm-3">
              <h5 className="text-secondary">Antipolo</h5>
              <ul className="text-secondary">
                <li>
                  C.lawis extension
                  <br />
                  9984 Street
                  <br />
                  Brgy. Asus
                  <br />
                  Antipolo City
                </li>
                <br />
                <li>
                  8-666-1234
                  <br />
                  8-123-654
                </li>
                <li>0906-2148748</li>
              </ul>
            </div>
            <div className="col-sm-3">
              <h5 className="text-secondary">Angono</h5>
              <ul className="text-secondary">
                <li>
                  Art Capital
                  <br />
                  1030 Steet
                  <br />
                  Brgy. MSI
                  <br />
                  Angono Rizal
                </li>
                <br />
                <li>
                  8-999-543
                  <br />
                  8-222-132
                </li>
                <li>0906-523241</li>
              </ul>
            </div>
            <div className="col-sm-3">
              <h5 className="text-secondary">Taytay</h5>
              <ul className="text-secondary">
                <li>
                  taytay market
                  <br />
                  hiway 2000
                  <br />
                  Brgy. Lenovo
                  <br />
                  Cainta
                </li>
                <br />
                <li>
                  8-111-1344
                  <br />
                  8-222-654
                </li>
                <li>0906-2148748</li>
              </ul>
            </div>
            <hr />
          </div>
        </div>
      </section>
    </>
  );
}
