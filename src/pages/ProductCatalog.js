import Accordion from "react-bootstrap/Accordion";
import { Table, Container, Row, Col } from "react-bootstrap";

export default function AllCollapseExample() {
  return (
    <>
      <Container fluid>
        <Row>
          <Col xs={2}>
            <Table bordered className="m-5 " variant="dark">
              <thead>
                <tr>
                  <th className="text-center">FILTER</th>
                </tr>
              </thead>
              <tbody></tbody>
            </Table>
          </Col>
          <Col xs={10}>
            <Table bordered className="m-5" variant="dark">
              <thead>
                <tr>
                  <th className="text-center">(+)</th>
                  <th className="text-center">CATEGORY</th>
                  <th>PRODUCT NAME</th>
                  <th className="text-center">PRICE</th>
                </tr>
              </thead>
              <tbody></tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </>
  );
}
