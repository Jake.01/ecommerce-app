import Banner from "../components/Banner.js";
import { react, useState, useEffect } from "react";
import "../App.css";
import {
  Carousel,
  Row,
  Container,
  Col,
  Stack,
  Card,
  CardGroup,
} from "react-bootstrap";
import "../App.css";

export default function Footer() {
  return (
    <section className="footer">
      <h1 className="text-center">footer</h1>
      <div className="container">
        <div className="row">
          <div className="col-sm-3">
            <h5 className="text-secondary">Makati</h5>
            <ul className="text-secondary">
              <li>
                Poblacion Makati
                <br />
                9984 Street
                <br />
                Brgy. Dell
                <br />
                MakatiCity
              </li>
              <br />
              <li>
                8-888-1234
                <br />
                8-999-654
              </li>
              <li>0906-2148748</li>
            </ul>
          </div>
          <div className="col-sm-3">
            <h5 className="text-secondary">Antipolo</h5>
            <ul className="text-secondary">
              <li>
                C.lawis extension
                <br />
                9984 Street
                <br />
                Brgy. Asus
                <br />
                Antipolo City
              </li>
              <br />
              <li>
                8-666-1234
                <br />
                8-123-654
              </li>
              <li>0906-2148748</li>
            </ul>
          </div>
          <div className="col-sm-3">
            <h5 className="text-secondary">Angono</h5>
            <ul className="text-secondary">
              <li>
                Art Capital
                <br />
                1030 Steet
                <br />
                Brgy. MSI
                <br />
                Angono Rizal
              </li>
              <br />
              <li>
                8-999-543
                <br />
                8-222-132
              </li>
              <li>0906-523241</li>
            </ul>
          </div>
          <div className="col-sm-3">
            <h5 className="text-secondary">Taytay</h5>
            <ul className="text-secondary">
              <li>
                taytay market
                <br />
                hiway 2000
                <br />
                Brgy. Lenovo
                <br />
                Cainta
              </li>
              <br />
              <li>
                8-111-1344
                <br />
                8-222-654
              </li>
              <li>0906-2148748</li>
            </ul>
          </div>
          <hr />
        </div>
      </div>
    </section>
  );
}
